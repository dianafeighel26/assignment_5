import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'

class RobotForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            type: '',
            mass: ''
        };
    }
    
     handleOnChangeName = (event) => {
        this.setState({
            name: event.target.value
        });
    }
     handleOnChangeType= (event) => {
        this.setState({
            type: event.target.value
        });
    }
     handleOnChangeMass = (event) => {
        this.setState({
            mass: event.target.value
        });
    }

    addRobot=(e)=>{
        e.preventDefault();
        const robot = this.state;
        this.props.onAdd(robot);
    }
    

  render() {
  	let {item} = this.props
    return (
      <div>
  	     <form>
             Name:
         <input type="text" id="name"  value = {this.state.name} onChange = {this.handleOnChangeName}/>
         Type:
         <input type="text" id="type" value = {this.state.type} onChange = {this.handleOnChangeType} />
         Mass:
         <input type="text" id="mass"  value = {this.state.mass} onChange = {this.handleOnChangeMass}/>
         <input type="submit" id="add" value="add" onClick={this.addRobot}/>
        </form>
      </div>
    )
  }
}

export default RobotForm
